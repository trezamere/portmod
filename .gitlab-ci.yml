image: registry.gitlab.com/portmod/portmod/rust:latest

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  PYLINTRC: ".pylintrc"

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  paths:
    - .cache
    - .venv/
    - ~/.cache/portmod
    - ~/.local/share/portmod/repos
    - target

stages:
  - build
  - test
  - deploy

before_script:
  - python -V # Print out python version for debugging
  - rustc -V
  - virtualenv .venv
  - source .venv/bin/activate

inquisitor:
  stage: test
  needs: ["install"]
  script:
  - mkdir -p .cache/tmp
  - pip install -e .[test] --cache-dir .cache/pip
  - portmod sync openmw
  - python ${CI_PROJECT_DIR}/bin/inquisitor scan ~/.local/share/portmod/repos/openmw

install:
  stage: build
  script:
  - mkdir -p .cache/tmp
  - pip install -e .[test] --cache-dir .cache/pip
  artifacts:
    paths:
      - portmodlib/portmod.cpython-*.so

lint:
  stage: test
  needs: ["install"]
  script:
  - pip install --upgrade flake8 pylint setuptools_rust
  - shopt -s globstar
  - flake8 --exclude .venv,--max-line-length=88 --filename '*.py,*.pybuild'
  - pylint -E **/*.py
  - cargo clippy -- -D warnings

pytest:
  stage: test
  needs: ["install"]
  script:
    - coverage run -m pytest --log-level NOTSET
    - coverage report
    - coverage xml
  coverage: '/TOTAL.*\s([.\d]+)%/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

mypy:
  stage: test
  needs: []
  script:
  - shopt -s globstar
  - pip install --upgrade mypy
  - mypy --install-types --non-interactive --incremental --cache-dir .cache/mypy portmod/**/*.py pybuild/**/*.py portmodlib/**/*.py

format:
  stage: test
  needs: []
  script:
  - pip install --upgrade black isort
  - shopt -s globstar
  - black --diff --check **/*.{py,pybuild}
  - cargo fmt -- --check -l
  - isort --diff --check portmod pybuild portmodlib bin

deploy:
  image: quay.io/pypa/manylinux2014_x86_64
  stage: deploy
  only:
  - tags
  before_script: []
  script: |
    curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain stable -y
    export PATH="/opt/python/cp36-cp36m/bin:$HOME/.cargo/bin:$PATH"
    pip install -U twine "setuptools!=50.0" setuptools_rust wheel
    pip install .[man]
    python setup.py build_rust --inplace --release build_man
    # Remove in-place library so it doesn't get included in the wheel
    rm portmodlib/portmod.cpython-36-x86_64-linux-gnu.so
    python setup.py bdist_wheel --py-limited-api=cp36
    python setup.py check sdist
    for whl in dist/*.whl; do
        auditwheel repair "$whl" -w dist/
        rm $whl
    done
    python -m twine upload dist/*

sast:
  inherit:
    default: false
  stage: test

gemnasium-python-dependency_scanning:
  inherit:
    default: false
  stage: test

include:
- template: Security/SAST.gitlab-ci.yml
- template: Dependency-Scanning.gitlab-ci.yml

doc:
  stage: test
  script:
    - pip install --upgrade sphinx sphinx-autodoc-typehints sphinx_rtd_theme sphinx-argparse
    - make -C doc clean
    - make -C doc html man
    - ./mkdoc.sh

pages:
  stage: deploy
  extends: doc
  after_script:
    - mv doc/_build/html/ public/
    - mv apidoc/_build/ public/api
  artifacts:
    paths:
      - public
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
