Contributing to Portmod
=======================

Note that some of this information refers to development of Portmod itself,
while other parts refers to the creation of portmod packages and package
repositories.

.. toctree::
   :maxdepth: 2

   packages
   l10n
   setup
