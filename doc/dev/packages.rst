Package Development
-------------------

.. toctree::
   :maxdepth: 2

   guidelines
   manifest
   use-flags
   modules
   archives
